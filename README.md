##A9 Atlassian Performance Profile Turnover

This is a Confluence export of our Turnover template.

You can load this XML document into your own Confluence instance to bootstrap a DIY performance evaluation.

Have a Confluence System Administrator restore the a9-performance-diy.zip to a space in your own Confluence 5.10 or greater instance.

* See https://confluence.atlassian.com/doc/restoring-a-space-152036.html


You can use the resulting page to:

- Review the performance evaluation tasks

- Populate your own JIRA project with the tasks for JIRA, Confluence, Bitbucket, Bamboo, HipChat, Crowd, Network and Storage performance evaluation
